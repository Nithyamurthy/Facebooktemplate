
import React, { Component } from 'react';


import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Platform,
  ActivityIndicator,
  Image,
  MenuProvider,
  MenuTrigger,
  MenuOptions,
  MenuOption,
  Menu,
} from 'react-native';


export default class App extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      serverData: [],
      fetching_from_server: false,
    };
    this.offset = 1;
    
  }

  componentDidMount() {
    fetch('https://randomuser.me/api/?results=10&seed=abc&page=' + this.offset)
      .then(response => response.json())
      .then(responseJson => {
        this.offset = this.offset + 1;
        this.setState({
          serverData: [...this.state.serverData, ...responseJson.results],
          loading: false,
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  loadMoreData = () => {
  
    this.setState({ fetching_from_server: true }, () => {
      fetch('https://randomuser.me/api/?results=10&seed=abc&page=' + this.offset)
          .then(response => response.json())
          .then(responseJson => { 
            this.offset = this.offset + 1;
            this.setState({
              serverData: [...this.state.serverData, ...responseJson.results],
              fetching_from_server: false,
            });
          })
          .catch(error => {
            console.error(error);
          });
    });
  };

  renderFooter() {
    return (
      <View style={styles.footer}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={this.loadMoreData}
          
          style={styles.loadMoreBtn}>
          <Text style={styles.btnText}>Load More</Text>
          {this.state.fetching_from_server ? (
            <ActivityIndicator color="white" style={{ marginLeft: 8 }} />
          ) : null}
        </TouchableOpacity>
      </View>
    );
  }

  menus() {
    return(
      <View>
      <MenuProvider>
      <Menu onSelect={value => alert(`You Clicked : ${value}`)}>
        <MenuTrigger >
        <Image source={require('./images/download.png')} style = {{height: 50, width: 50,justifyContent: 'flex-end', resizeMode : 'stretch',}} />
        </MenuTrigger >
        <MenuOptions>
          <MenuOption value={"Edit"}>
            <Text>Edit</Text>
          </MenuOption>
          <MenuOption value={"Remove"}>
            <Text>Remove</Text>
          </MenuOption>
          <MenuOption value={"Share"}>
            <Text>Share</Text>
          </MenuOption>
        </MenuOptions>
      </Menu>
    </MenuProvider>
    </View> 
    );
  }

  render() {
    let day = new Date().getDate(); //Current Date
    let month = new Date().getMonth() + 1; //Current Month
    let year = new Date().getFullYear();//Current Year
    let hours = new Date().getHours();
    let mins = new Date().getMinutes();
    let secs = new Date().getSeconds();
    return (
      <View style={styles.MainContainer}>
        {this.state.loading ? (
          <ActivityIndicator size="large" />
        ) : (
          <FlatList
            style={{ width: '100%' }}
            keyExtractor={(item, index) => index.toString()}
            data={this.state.serverData}
            renderItem={({ item, index }) => (
              <View style={styles.item}>
                
                <View style={styles.subcontainer}>
    <View style={styles.newcontainer}>
      <Image source={{uri: item.picture.thumbnail}} style = {{height: 50, width: 50, resizeMode : 'contain',}} />
      <Text style={styles.text1}>{item.name.first}{" "}{item.name.last} {"\n"}  
      <Text style={{fontSize: 10, color: 'grey'}}>{day}:{month}:{year} at {hours}:{mins}:{secs}</Text></Text>
      

    </View> 
    <Text style={{fontFamily: 'Roboto',fontSize: 20}}>{item.email}</Text>
    <Image source={{uri: item.picture.large}}  style = {{height: 250, width: '100%', resizeMode: 'contain'}}></Image>
        </View>   
              </View>
            )}
            ItemSeparatorComponent={() => <View style={styles.separator} />}
            ListFooterComponent={this.renderFooter.bind(this)}
            MenuComponent ={this.menus.bind(this)}
            
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
    color: 'blue',
  },
  item: {
    padding: 10,
  },
  separator: {
    height: 0.5,
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  text: {
    fontSize: 15,
    color: 'black',
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#800000',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
  text1: {
    fontWeight: 'bold',
    color: 'blue',
    fontSize: 20,
    fontFamily: 'Roboto',

  },
  text: {
    fontFamily: 'Roboto',
    fontSize: 15,
    
    
  },
  
separator: {
borderBottomColor: '#bbb',
borderBottomWidth: StyleSheet.hairlineWidth,
},
newcontainer: {
flex: 1,
alignContent: 'flex-start',
flexDirection: 'row',
},
title: {
fontWeight: 'bold',
fontSize: 15,
color: 'black',

},
rightButton: {
width: 100,
height: 37,
position: 'absolute',
bottom: 8,
right: 2,
padding: 8
},
subcontainer: {
  backgroundColor: 'white',
  flex: 2,
},
MainContainer: {
  flex:1,
  backgroundColor: 'blue',
},

});

